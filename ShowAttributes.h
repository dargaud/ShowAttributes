#ifndef __SHOW_ATTRIBUTES_H
#define __SHOW_ATTRIBUTES_H

#include <stdio.h>

extern void ShowAttributes(FILE* File, int Panel, int Control);
extern void GetAttributes(char* Dest, unsigned int Size, int Panel, int Control);

#endif
