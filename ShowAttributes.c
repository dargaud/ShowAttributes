#include <string.h>
#include <iso646.h>

#include <userint.h>
#include <utility.h>

#include "Def.h"
#include "ShowAttributes.h"

static int	FirstAttr=ATTR_DIMMED, 
			LastAttr=ATTR_USE_PROGRESS_BAR_VISUAL_STYLES;

typedef struct sAttrList {
	int Attribute;
	char *NameStr, *TypeStr;
	int IsPanelAttr;
} tAttrList;

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-field-initializers"

// Generate this list with:
// grep "#define ATTR_" userint.h | tr "\t" " " | grep -v "  ATTR_" | sed -e "s%#define ATTR_\([^ ]*\)[ 0-9]*/\* %{ATTR_\1, \"\L\1\", \"%g" -e "s% \*/%\"},%"
static tAttrList AttrList[]={
	{ATTR_DIMMED, "dimmed", "int (boolean)"},
	{ATTR_CONSTANT_NAME, "constant_name", "(char *) (not settable)"},
	{ATTR_CONSTANT_NAME_LENGTH, "constant_name_length", "int (not settable)"},
	{ATTR_CALLBACK_DATA, "callback_data", "(void *)"},
	{ATTR_CALLBACK_NAME, "callback_name", "(char *) (not settable)"},
	{ATTR_CALLBACK_NAME_LENGTH, "callback_name_length", "int (not settable)"},
	{ATTR_CALLBACK_FUNCTION_POINTER, "callback_function_pointer", "(void *)"},
	{ATTR_VISIBLE, "visible", "int (boolean)"},
	{ATTR_LEFT, "left", "int (-32768 - 32767)"},
	{ATTR_TOP, "top", "int (-32768 - 32767)"},
	{ATTR_WIDTH, "width", "int (0-32767)"},
	{ATTR_HEIGHT, "height", "int (0-32767)"},
	{ATTR_FRAME_COLOR, "frame_color", "int (rgb value)"},
	{ATTR_SCROLL_BARS, "scroll_bars", "int: VAL_NO_SCROLL_BARS, ..."},
	{ATTR_SCROLL_BAR_COLOR, "scroll_bar_color", "int (rgb value)"},
	{ATTR_HSCROLL_OFFSET, "hscroll_offset", "int (in pixels)"},
	{ATTR_HSCROLL_OFFSET_MAX, "hscroll_offset_max", "int (in pixels)"},
	{ATTR_CHART_HSCROLL_OFFSET, "chart_hscroll_offset", "int (in points)"},
	{ATTR_BACKCOLOR, "backcolor", "int (rgb value)"},
	{ATTR_TITLEBAR_VISIBLE, "titlebar_visible", "int (boolean)"},
	{ATTR_TITLEBAR_THICKNESS, "titlebar_thickness", "int"},
	{ATTR_TITLEBAR_ACTUAL_THICKNESS, "titlebar_actual_thickness", "int (not settable)"},
	{ATTR_TITLE, "title", "(char *)"},
	{ATTR_TITLE_LENGTH, "title_length", "int (not settable)"},
	{ATTR_TITLE_COLOR, "title_color", "int (rgb value)"},
	{ATTR_TITLE_BACKCOLOR, "title_backcolor", "int (rgb value)"},
	{ATTR_FRAME_STYLE, "frame_style", "int: VAL_OUTLINED_FRAME, ..."},
	{ATTR_FRAME_THICKNESS, "frame_thickness", "int"},
	{ATTR_FRAME_ACTUAL_WIDTH, "frame_actual_width", "int (not settable)"},
	{ATTR_FRAME_ACTUAL_HEIGHT, "frame_actual_height", "int (not settable)"},
	{ATTR_MENU_HEIGHT, "menu_height", "int"},
	{ATTR_MENU_WIDTH, "menu_width", "int"},
	{ATTR_SIZABLE, "sizable", "int (boolean)"},
	{ATTR_MOVABLE, "movable", "int (boolean)"},
	{ATTR_NUM_CTRLS, "num_ctrls", "int (not settable)"},
	{ATTR_MOUSE_CURSOR, "mouse_cursor", "int: VAL_DEFAULT_CURSOR, ..."},
	{ATTR_TITLE_FONT, "title_font", "(char *)"},
	{ATTR_TITLE_CHARACTER_SET, "title_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_TITLE_ITALIC, "title_italic", "int (boolean)"},
	{ATTR_TITLE_UNDERLINE, "title_underline", "int (boolean)"},
	{ATTR_TITLE_STRIKEOUT, "title_strikeout", "int (boolean)"},
	{ATTR_TITLE_POINT_SIZE, "title_point_size", "int (1-255)"},
	{ATTR_TITLE_BOLD, "title_bold", "int (boolean)"},
	{ATTR_TITLE_FONT_NAME_LENGTH, "title_font_name_length", "int (not settable)"},
	{ATTR_MENU_BAR_VISIBLE, "menu_bar_visible", "int (boolean)"},
	{ATTR_CLOSE_CTRL, "close_ctrl", "int (ctrl Id)"},
	{ATTR_VSCROLL_OFFSET, "vscroll_offset", "int (in pixels)"},
	{ATTR_VSCROLL_OFFSET_MAX, "vscroll_offset_max", "int (in pixels)"},
	{ATTR_PARENT_SHARES_SHORTCUT_KEYS, "parent_shares_shortcut_keys", "int (boolean)"},
	{ATTR_ACTIVATE_WHEN_CLICKED_ON, "activate_when_clicked_on", "int (boolean)"},
	{ATTR_WINDOW_ZOOM, "window_zoom", "int: VAL_MINIMIZE, ..."},
	{ATTR_SYSTEM_WINDOW_HANDLE, "system_window_handle", "intptr_t (HWND, or X-Window)"},
	{ATTR_CAN_MINIMIZE, "can_minimize", "int (boolean)"},
	{ATTR_CAN_MAXIMIZE, "can_maximize", "int (boolean)"},
	{ATTR_CLOSE_ITEM_VISIBLE, "close_item_visible", "int (boolean)"},
	{ATTR_SYSTEM_MENU_VISIBLE, "system_menu_visible", "int (boolean)"},
	{ATTR_PANEL_MENU_BAR_CONSTANT, "panel_menu_bar_constant", "(char *)"},
	{ATTR_PANEL_MENU_BAR_CONSTANT_LENGTH, "panel_menu_bar_constant_length", "int (not settable)"},
	{ATTR_TITLE_SIZE_TO_FONT, "title_size_to_font", "int (boolean)"},
	{ATTR_PANEL_PARENT, "panel_parent", "int (not settable)"},
	{ATTR_NUM_CHILDREN, "num_children", "int (not settable)"},
	{ATTR_FIRST_CHILD, "first_child", "int (not settable)"},
	{ATTR_NEXT_PANEL, "next_panel", "int (not settable)"},
	{ATTR_ZPLANE_POSITION, "zplane_position", "int (0 - numObjects-1)"},
	{ATTR_CTRL_STYLE, "ctrl_style", "int: CTRL_NUMERIC, ... (not settable)"},
	{ATTR_CTRL_TAB_POSITION, "ctrl_tab_position", "int"},
	{ATTR_PANEL_FIRST_CTRL, "panel_first_ctrl", "int (not settable)"},
	{ATTR_NEXT_CTRL, "next_ctrl", "int (not settable)"},
	{ATTR_ACTIVE, "active", "int (not settable)"},
	{ATTR_FLOATING, "floating", "int VAL_FLOAT_NEVER, ..."},
	{ATTR_TL_ACTIVATE_WHEN_CLICKED_ON, "tl_activate_when_clicked_on", "int (boolean)"},
	{ATTR_CONFORM_TO_SYSTEM, "conform_to_system", "int (boolean)"},
	{ATTR_SCALE_CONTENTS_ON_RESIZE, "scale_contents_on_resize", "int (boolean)"},
	{ATTR_MIN_HEIGHT_FOR_SCALING, "min_height_for_scaling", "int (0-32767)"},
	{ATTR_CTRL_MODE, "ctrl_mode", "int: VAL_HOT, ..."},
	{ATTR_MIN_WIDTH_FOR_SCALING, "min_width_for_scaling", "int (0-32767)"},
	{ATTR_HAS_TASKBAR_BUTTON, "has_taskbar_button", "int (boolean)"},
	{ATTR_OWNER_THREAD_ID, "owner_thread_id", "int (not settable)"},
	{ATTR_SCROLL_BAR_STYLE, "scroll_bar_style", "int: VAL_CLASSIC, ..."},
	{ATTR_MINIMIZE_OTHER_PANELS, "minimize_other_panels", "int (boolean)"},
	{ATTR_CTRL_VAL, "ctrl_val", "same datatype as ctrl"},
	{ATTR_LABEL_COLOR, "label_color", "int (rgb value)"},
	{ATTR_LABEL_TEXT, "label_text", "(char *)"},
	{ATTR_LABEL_FONT, "label_font", "(char *)"},
	{ATTR_LABEL_ITALIC, "label_italic", "int (boolean)"},
	{ATTR_LABEL_UNDERLINE, "label_underline", "int (boolean)"},
	{ATTR_LABEL_STRIKEOUT, "label_strikeout", "int (boolean)"},
	{ATTR_LABEL_POINT_SIZE, "label_point_size", "int (1-255)"},
	{ATTR_LABEL_BOLD, "label_bold", "int (boolean)"},
	{ATTR_LABEL_ANGLE, "label_angle", "int (-32768 - 32767)"},
	{ATTR_LABEL_TEXT_LENGTH, "label_text_length", "int (not settable)"},
	{ATTR_LABEL_SIZE_TO_TEXT, "label_size_to_text", "int (boolean)"},
	{ATTR_LABEL_FONT_NAME_LENGTH, "label_font_name_length", "int (not settable)"},
	{ATTR_LABEL_CHARACTER_SET, "label_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_LABEL_VISIBLE, "label_visible", "int (boolean)"},
	{ATTR_LABEL_LEFT, "label_left", "int (-32768 - 32767)"},
	{ATTR_LABEL_TOP, "label_top", "int (-32768 - 32767)"},
	{ATTR_LABEL_WIDTH, "label_width", "int (0-32767)"},
	{ATTR_LABEL_HEIGHT, "label_height", "int (0-32767)"},
	{ATTR_LABEL_BGCOLOR, "label_bgcolor", "int (rgb value)"},
	{ATTR_LABEL_JUSTIFY, "label_justify", "int: VAL_LEFT_JUSTIFIED, ..."},
	{ATTR_LABEL_RAISED, "label_raised", "int (boolean)"},
//	{ATTR_TOOLTIP_TEXT, "tooltip_text", "(char *)"},
	{ATTR_TOOLTIP_TEXT_LENGTH, "tooltip_text_length", "int (not settable)"},
	{ATTR_TEXT_COLOR, "text_color", "int (rgb value)"},
	{ATTR_TEXT_FONT_NAME_LENGTH, "text_font_name_length", "int (not settable)"},
	{ATTR_TEXT_FONT, "text_font", "(char *)"},
	{ATTR_TEXT_ITALIC, "text_italic", "int (boolean)"},
	{ATTR_TEXT_UNDERLINE, "text_underline", "int (boolean)"},
	{ATTR_TEXT_STRIKEOUT, "text_strikeout", "int (boolean)"},
	{ATTR_TEXT_POINT_SIZE, "text_point_size", "int (1-255)"},
	{ATTR_TEXT_BOLD, "text_bold", "int (boolean)"},
	{ATTR_TEXT_ANGLE, "text_angle", "int (-32768 - 32767)"},
	{ATTR_TEXT_CHARACTER_SET, "text_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_TEXT_BGCOLOR, "text_bgcolor", "int (rgb value)"},
	{ATTR_TEXT_JUSTIFY, "text_justify", "int: VAL_LEFT_JUSTIFIED, ..."},
	{ATTR_DATA_TYPE, "data_type", "int: VAL_CHAR, ..."},
	{ATTR_CTRL_INDEX, "ctrl_index", "int: (0 to numItems-1)"},
	{ATTR_DFLT_INDEX, "dflt_index", "int: (0 to numItems-1)"},
	{ATTR_MAX_VALUE, "max_value", "same datatype as ctrl"},
	{ATTR_MIN_VALUE, "min_value", "same datatype as ctrl"},
	{ATTR_DFLT_VALUE, "dflt_value", "same datatype as ctrl"},
	{ATTR_INCR_VALUE, "incr_value", "same datatype as ctrl"},
	{ATTR_FORMAT, "format", "int: VAL_HEX_FORMAT, ..."},
	{ATTR_PRECISION, "precision", "int (0 - 32)"},
	{ATTR_SHOW_RADIX, "show_radix", "int (boolean)"},
	{ATTR_SHOW_INCDEC_ARROWS, "show_incdec_arrows", "int (boolean)"},
	{ATTR_CHECK_RANGE, "check_range", "int: VAL_COERCE, ..."},
	{ATTR_PADDING, "padding", "int: (0 - 64)"},
	{ATTR_DFLT_VALUE_LENGTH, "dflt_value_length", "int (not settable)"},
	{ATTR_DISABLE_RADIX, "disable_radix", "int (boolean)"},
	{ATTR_INCDEC_WIDTH, "incdec_width", "int (0-32767)"},
	{ATTR_SLIDE_INCDEC_LENGTH, "slide_incdec_length", "int (0-32767)"},
	{ATTR_MENU_ARROW_WIDTH, "menu_arrow_width", "int (0-32767)"},
	{ATTR_MENU_ARROW_HEIGHT, "menu_arrow_height", "int (0-32767)"},
	{ATTR_DISABLE_CHECK_MARK, "disable_check_mark", "int (boolean)"},
	{ATTR_MAX_ENTRY_LENGTH, "max_entry_length", "int (-1 means no limit)"},
	{ATTR_MAX_ENTRY_CHARS, "max_entry_chars", "int (-1 means no limit)"},
	{ATTR_TEXT_SELECTION_START, "text_selection_start", "int"},
	{ATTR_TEXT_SELECTION_LENGTH, "text_selection_length", "int"},
	{ATTR_ENABLE_CHARACTER_MASKING, "enable_character_masking", "int (boolean)"},
	{ATTR_MASK_CHARACTER, "mask_character", "int (ASCII character)"},
	{ATTR_STRING_TEXT_LENGTH, "string_text_length", "int (not settable)"},
	{ATTR_FIRST_VISIBLE_LINE, "first_visible_line", "int"},
	{ATTR_WRAP_MODE, "wrap_mode", "int: VAL_CHAR_WRAP, ..."},
	{ATTR_EXTRA_LINES, "extra_lines", "int (-1 means no limit)"},
	{ATTR_TOTAL_LINES, "total_lines", "int (not settable)"},
	{ATTR_ENTER_IS_NEWLINE, "enter_is_newline", "int (boolean)"},
	{ATTR_CONFORM_TO_SYSTEM_THEME, "conform_to_system_theme", "int (boolean)"},
	{ATTR_DISABLE_PANEL_THEME, "disable_panel_theme", "int (boolean)"},
	{ATTR_SCROLL_BAR_SIZE, "scroll_bar_size", "int: VAL_SMALL_SCROLL_BARS, ... (not settable for panels )"},
	{ATTR_VISIBLE_LINES, "visible_lines", "int (1 - 500)"},
	{ATTR_NO_EDIT_TEXT, "no_edit_text", "int (boolean)"},
	{ATTR_TEXT_RAISED, "text_raised", "int (boolean)"},
	{ATTR_SIZE_TO_TEXT, "size_to_text", "int (boolean)"},
	{ATTR_CMD_BUTTON_COLOR, "cmd_button_color", "int (rgb value)"},
	{ATTR_AUTO_SIZING, "auto_sizing", "int: VAL_ALWAYS_AUTO_SIZE, ..."},
	{ATTR_ON_VALUE, "on_value", "same datatype as ctrl"},
	{ATTR_OFF_VALUE, "off_value", "same datatype as ctrl"},
	{ATTR_ON_VALUE_LENGTH, "on_value_length", "int (only if string value) (not settable)"},
	{ATTR_OFF_VALUE_LENGTH, "off_value_length", "int (only if string value) (not settable)"},
	{ATTR_BINARY_SWITCH_COLOR, "binary_switch_color", "int (rgb value)"},
	{ATTR_ON_COLOR, "on_color", "int (rgb value)"},
	{ATTR_OFF_COLOR, "off_color", "int (rgb value)"},
	{ATTR_ON_TEXT, "on_text", "(char *)"},
	{ATTR_OFF_TEXT, "off_text", "(char *)"},
	{ATTR_ON_TEXT_LENGTH, "on_text_length", "int (not settable)"},
	{ATTR_OFF_TEXT_LENGTH, "off_text_length", "int (not settable)"},
	{ATTR_DIG_DISP_TOP, "dig_disp_top", "int (-32768 - 32767)"},
	{ATTR_DIG_DISP_LEFT, "dig_disp_left", "int (-32768 - 32767)"},
	{ATTR_DIG_DISP_WIDTH, "dig_disp_width", "int (0-32767)"},
	{ATTR_DIG_DISP_HEIGHT, "dig_disp_height", "int (0-32767)"},
	{ATTR_SHOW_DIG_DISP, "show_dig_disp", "int (boolean)"},
	{ATTR_SLIDER_COLOR, "slider_color", "int (rgb value)"},
	{ATTR_FILL_HOUSING_COLOR, "fill_housing_color", "int (rgb value)"},
	{ATTR_MARKER_STYLE, "marker_style", "int: VAL_NO_MARKERS, ..."},
	{ATTR_TICK_STYLE, "tick_style", "int: VAL_NO_TICKS, ..."},
	{ATTR_FILL_COLOR, "fill_color", "int (rgb value)"},
	{ATTR_FILL_OPTION, "fill_option", "int: VAL_NO_FILL, ..."},
	{ATTR_MARKER_START_ANGLE, "marker_start_angle", "int (0-359)"},
	{ATTR_MARKER_END_ANGLE, "marker_end_angle", "int (0-359)"},
	{ATTR_SLIDER_WIDTH, "slider_width", "int (0-32767)"},
	{ATTR_SLIDER_HEIGHT, "slider_height", "int (0-32767)"},
	{ATTR_FORCE_SMOOTH_DRAGGING, "force_smooth_dragging", "int (boolean)"},
	{ATTR_SHOW_MORE_BUTTON, "show_more_button", "int (boolean)"},
	{ATTR_SHOW_TRANSPARENT, "show_transparent", "int (boolean)"},
	{ATTR_SLIDER_LEFT, "slider_left", "int (0-32767)"},
	{ATTR_SLIDER_TOP, "slider_top", "int (0-32767)"},
	{ATTR_NUM_DIVISIONS, "num_divisions", "int (1-100, or VAL_AUTO)"},
	{ATTR_MENU_ARROW_COLOR, "menu_arrow_color", "int (rgb value)"},
	{ATTR_MENU_BAR_POINT_SIZE, "menu_bar_point_size", "int (1-255)"},
	{ATTR_MENU_BAR_BOLD, "menu_bar_bold", "int (boolean)"},
	{ATTR_MENU_BAR_ITALIC, "menu_bar_italic", "int (boolean)"},
	{ATTR_MENU_BAR_UNDERLINE, "menu_bar_underline", "int (boolean)"},
	{ATTR_MENU_BAR_STRIKEOUT, "menu_bar_strikeout", "int (boolean)"},
	{ATTR_MENU_BAR_FONT, "menu_bar_font", "(char *)"},
	{ATTR_MENU_BAR_FONT_NAME_LENGTH, "menu_bar_font_name_length", "int (not settable)"},
	{ATTR_MENU_BAR_CHARACTER_SET, "menu_bar_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_MENU_IMAGE_BACKGROUND_COLOR, "menu_image_background_color", "int (rgb value)"},
	{ATTR_MENU_BAR_IMAGE_SIZE, "menu_bar_image_size", "int (not settable)"},
	{ATTR_SHOW_IMMEDIATE_ACTION_SYMBOL, "show_immediate_action_symbol", "int (boolean)"},
	{ATTR_SHORTCUT_KEY, "shortcut_key", "int"},
	{ATTR_CHECKED, "checked", "int (boolean)"},
	{ATTR_IS_SEPARATOR, "is_separator", "int (boolean) (not settable)"},
	{ATTR_ITEM_NAME, "item_name", "(char *)"},
	{ATTR_ITEM_NAME_LENGTH, "item_name_length", "int (not settable)"},
	{ATTR_SUBMENU_ID, "submenu_id", "int (not settable)"},
	{ATTR_NEXT_ITEM_ID, "next_item_id", "int (not settable)"},
	{ATTR_BOLD, "bold", "int (boolean)"},
	{ATTR_IMAGE_FILE, "image_file", "(char *)"},
	{ATTR_IMAGE_FILE_LENGTH, "image_file_length", "int (not settable)"},
	{ATTR_FAST_DRAW_BUTTON, "fast_draw_button", "int (boolean)"},
	{ATTR_USE_SUBIMAGE, "use_subimage", "int (boolean)"},
	{ATTR_SUBIMAGE_TOP, "subimage_top", "int (positive or zero)"},
	{ATTR_SUBIMAGE_LEFT, "subimage_left", "int (positive or zero)"},
	{ATTR_SUBIMAGE_WIDTH, "subimage_width", "int (positive or zero)"},
	{ATTR_SUBIMAGE_HEIGHT, "subimage_height", "int (positive or zero)"},
	{ATTR_MENU_NAME, "menu_name", "(char *)"},
	{ATTR_MENU_NAME_LENGTH, "menu_name_length", "int (not settable)"},
	{ATTR_NUM_MENU_ITEMS, "num_menu_items", "int (not settable)"},
	{ATTR_NEXT_MENU_ID, "next_menu_id", "int (not settable)"},
	{ATTR_FIRST_ITEM_ID, "first_item_id", "int (not settable)"},
	{ATTR_NUM_MENUS, "num_menus", "int (not settable)"},
	{ATTR_DRAW_LIGHT_BEVEL, "draw_light_bevel", "int (boolean)"},
	{ATTR_DIMMER_CALLBACK, "dimmer_callback", "(void *) (really a MenuDimmerCallbackPtr)"},
	{ATTR_FIRST_MENU_ID, "first_menu_id", "int (not settable)"},
	{ATTR_SEND_DIMMER_EVENTS_FOR_ALL_KEYS, "send_dimmer_events_for_all_keys", "int (boolean)"},
	{ATTR_GRID_COLOR, "grid_color", "int (rgb value)"},
	{ATTR_PLOT_BGCOLOR, "plot_bgcolor", "int (rgb value)"},
	{ATTR_XYNAME_FONT, "xyname_font", "(char *)"},
	{ATTR_XYNAME_CHARACTER_SET, "xyname_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_XYNAME_COLOR, "xyname_color", "int (rgb value)"},
	{ATTR_XYLABEL_FONT, "xylabel_font", "(char *)"},
	{ATTR_XYLABEL_COLOR, "xylabel_color", "int (rgb value)"},
	{ATTR_XNAME, "xname", "(char *)"},
	{ATTR_XGRID_VISIBLE, "xgrid_visible", "int (boolean)"},
	{ATTR_XLABEL_VISIBLE, "xlabel_visible", "int (boolean)"},
	{ATTR_XFORMAT, "xformat", "int: VAL_ENGINEERING..."},
	{ATTR_XDIVISIONS, "xdivisions", "int (1-100, or VAL_AUTO)"},
	{ATTR_XPRECISION, "xprecision", "int (0-15, or VAL_AUTO)"},
	{ATTR_XENG_UNITS, "xeng_units", "int (-308 to 308)"},
	{ATTR_YNAME, "yname", "(char *)"},
	{ATTR_YGRID_VISIBLE, "ygrid_visible", "int (boolean)"},
	{ATTR_YLABEL_VISIBLE, "ylabel_visible", "int (boolean)"},
	{ATTR_YMAP_MODE, "ymap_mode", "int: VAL_LINEAR or VAL_LOG"},
	{ATTR_YFORMAT, "yformat", "int: VAL_ENGINEERING..."},
	{ATTR_YDIVISIONS, "ydivisions", "int (1-100, or VAL_AUTO)"},
	{ATTR_YPRECISION, "yprecision", "int (0-15, or VAL_AUTO)"},
	{ATTR_YENG_UNITS, "yeng_units", "int (-308 to 308)"},
	{ATTR_EDGE_STYLE, "edge_style", "int: VAL_RAISED_EDGE, ..."},
	{ATTR_BORDER_VISIBLE, "border_visible", "int (boolean)"},
	{ATTR_XYNAME_BOLD, "xyname_bold", "int (boolean)"},
	{ATTR_XYNAME_ITALIC, "xyname_italic", "int (boolean)"},
	{ATTR_XYNAME_UNDERLINE, "xyname_underline", "int (boolean)"},
	{ATTR_XYNAME_STRIKEOUT, "xyname_strikeout", "int (boolean)"},
	{ATTR_XYNAME_POINT_SIZE, "xyname_point_size", "int (1-255)"},
	{ATTR_XNAME_LENGTH, "xname_length", "int (not settable)"},
	{ATTR_YNAME_LENGTH, "yname_length", "int (not settable)"},
	{ATTR_XYNAME_FONT_NAME_LENGTH, "xyname_font_name_length", "int (not settable)"},
	{ATTR_XYLABEL_BOLD, "xylabel_bold", "int (boolean)"},
	{ATTR_XYLABEL_ITALIC, "xylabel_italic", "int (boolean)"},
	{ATTR_XYLABEL_UNDERLINE, "xylabel_underline", "int (boolean)"},
	{ATTR_XYLABEL_STRIKEOUT, "xylabel_strikeout", "int (boolean)"},
	{ATTR_XYLABEL_POINT_SIZE, "xylabel_point_size", "int (1-255)"},
	{ATTR_XYLABEL_FONT_NAME_LENGTH, "xylabel_font_name_length", "int (not settable)"},
	{ATTR_GRAPH_BGCOLOR, "graph_bgcolor", "int (rgb value)"},
	{ATTR_PLOT_AREA_WIDTH, "plot_area_width", "int (not settable)"},
	{ATTR_PLOT_AREA_HEIGHT, "plot_area_height", "int (not settable)"},
	{ATTR_INNER_MARKERS_VISIBLE, "inner_markers_visible", "int (boolean)"},
	{ATTR_YREVERSE, "yreverse", "int (boolean)"},
	{ATTR_XUSE_LABEL_STRINGS, "xuse_label_strings", "int (boolean)"},
	{ATTR_YUSE_LABEL_STRINGS, "yuse_label_strings", "int (boolean)"},
	{ATTR_XAXIS_GAIN, "xaxis_gain", "double"},
	{ATTR_YAXIS_GAIN, "yaxis_gain", "double"},
	{ATTR_XAXIS_OFFSET, "xaxis_offset", "double"},
	{ATTR_YAXIS_OFFSET, "yaxis_offset", "double"},
	{ATTR_PLOT_AREA_TOP, "plot_area_top", "int (not settable)"},
	{ATTR_PLOT_AREA_LEFT, "plot_area_left", "int (not settable)"},
	{ATTR_XPADDING, "xpadding", "int (0 to 64)"},
	{ATTR_YPADDING, "ypadding", "int (0 to 64)"},
	{ATTR_ACTUAL_XDIVISIONS, "actual_xdivisions", "int (not settable)"},
	{ATTR_ACTUAL_YDIVISIONS, "actual_ydivisions", "int (not settable)"},
	{ATTR_ACTUAL_XPRECISION, "actual_xprecision", "int (not settable)"},
	{ATTR_ACTUAL_YPRECISION, "actual_yprecision", "int (not settable)"},
	{ATTR_XMINORGRID_VISIBLE, "xminorgrid_visible", "int (boolean)"},
	{ATTR_YMINORGRID_VISIBLE, "yminorgrid_visible", "int (boolean)"},
	{ATTR_FIXED_PLOT_AREA, "fixed_plot_area", "int (boolean)"},
	{ATTR_NUM_CURSORS, "num_cursors", "int (0-10)"},
	{ATTR_XMAP_MODE, "xmap_mode", "int: VAL_LINEAR or VAL_LOG"},
	{ATTR_DATA_MODE, "data_mode", "int: VAL_RETAIN, ..."},
	{ATTR_COPY_ORIGINAL_DATA, "copy_original_data", "int (boolean)"},
	{ATTR_XMARK_ORIGIN, "xmark_origin", "int (boolean)"},
	{ATTR_YMARK_ORIGIN, "ymark_origin", "int (boolean)"},
	{ATTR_SMOOTH_UPDATE, "smooth_update", "int (boolean)"},
	{ATTR_REFRESH_GRAPH, "refresh_graph", "int (boolean)"},
	{ATTR_SHIFT_TEXT_PLOTS, "shift_text_plots", "int (boolean)"},
	{ATTR_ACTIVE_YAXIS, "active_yaxis", "int: VAL_LEFT_YAXIS or VAL_RIGHT_YAXIS"},
	{ATTR_XREVERSE, "xreverse", "int (boolean)"},
	{ATTR_ENABLE_ZOOM_AND_PAN, "enable_zoom_and_pan", "int (boolean)"},
	{ATTR_XLOOSE_FIT_AUTOSCALING, "xloose_fit_autoscaling", "int (boolean)"},
	{ATTR_YLOOSE_FIT_AUTOSCALING, "yloose_fit_autoscaling", "int (boolean)"},
	{ATTR_XLOOSE_FIT_AUTOSCALING_UNIT, "xloose_fit_autoscaling_unit", "int (-308 to 308)"},
	{ATTR_YLOOSE_FIT_AUTOSCALING_UNIT, "yloose_fit_autoscaling_unit", "int (-308 to 308)"},
	{ATTR_ANTI_ALIASED_PLOTS, "anti_aliased_plots", "int (boolean)"},
	{ATTR_LEGEND_VISIBLE, "legend_visible", "int (boolean)"},
	{ATTR_LEGEND_TOP, "legend_top", "int (-32768 - 32767)"},
	{ATTR_LEGEND_LEFT, "legend_left", "int (-32768 - 32767)"},
	{ATTR_LEGEND_WIDTH, "legend_width", "int (-32768 - 32767)"},
	{ATTR_LEGEND_HEIGHT, "legend_height", "int (-32768 - 32767)"},
	{ATTR_LEGEND_FRAME_COLOR, "legend_frame_color", "int (rgb value)"},
	{ATTR_LEGEND_PLOT_BGCOLOR, "legend_plot_bgcolor", "int (rgb value)"},
	{ATTR_LEGEND_SHOW_SAMPLES, "legend_show_samples", "int (boolean)"},
	{ATTR_LEGEND_AUTO_SIZE, "legend_auto_size", "int (boolean)"},
	{ATTR_LEGEND_AUTO_DISPLAY, "legend_auto_display", "int (boolean)"},
	{ATTR_LEGEND_NUM_VISIBLE_ITEMS, "legend_num_visible_items", "int (0-500)"},
	{ATTR_ACTIVE_XAXIS, "active_xaxis", "int: VAL_BOTTOM_XAXIS or VAL_TOP_XAXIS"},
	{ATTR_LEGEND_INTERACTIVE, "legend_interactive", "int (boolean)"},
	{ATTR_NUM_TRACES, "num_traces", "int (1-64)"},
	{ATTR_POINTS_PER_SCREEN, "points_per_screen", "int (3-10000)"},
	{ATTR_SCROLL_MODE, "scroll_mode", "int: VAL_SWEEP, ..."},
	{ATTR_ENABLE_EDITABLE_AXES, "enable_editable_axes", "int (boolean)"},
	{ATTR_ZOOM_STYLE, "zoom_style", "int: ZOOM_AROUND_PT, ..."},
	{ATTR_CURSOR_MODE, "cursor_mode", "int: VAL_FREE_FORM, ..."},
	{ATTR_CURSOR_POINT_STYLE, "cursor_point_style", "int: VAL_EMPTY_SQUARE, ..."},
	{ATTR_CROSS_HAIR_STYLE, "cross_hair_style", "int: VAL_LONG_CROSS, ..."},
	{ATTR_CURSOR_COLOR, "cursor_color", "int (rgb value)"},
	{ATTR_CURSOR_YAXIS, "cursor_yaxis", "int: VAL_LEFT_YAXIS or VAL_RIGHT_YAXIS"},
	{ATTR_CURSOR_ENABLED, "cursor_enabled", "int (boolean)"},
	{ATTR_CURSOR_XAXIS, "cursor_xaxis", "int: VAL_BOTTOM_XAXIS or VAL_TOP_XAXIS"},
	{ATTR_TRACE_THICKNESS, "trace_thickness", "int (1-32)"},
	{ATTR_TRACE_COLOR, "trace_color", "int (rgb value)"},
	{ATTR_PLOT_STYLE, "plot_style", "int: VAL_THIN_LINE, ..."},
	{ATTR_TRACE_POINT_STYLE, "trace_point_style", "int: VAL_EMPTY_SQUARE, ..."},
	{ATTR_LINE_STYLE, "line_style", "int: VAL_SOLID, ..."},
	{ATTR_TRACE_VISIBLE, "trace_visible", "int (boolean)"},
	{ATTR_TRACE_YAXIS, "trace_yaxis", "int: VAL_LEFT_YAXIS or VAL_RIGHT_YAXIS"},
	{ATTR_HISTORY_BUFFER_SIZE, "history_buffer_size", "int: >= pointsPerScreen"},
	{ATTR_STRIP_CHART_PAUSED, "strip_chart_paused", "int (boolean)"},
	{ATTR_SWEEP_LINE_COLOR, "sweep_line_color", "int (rgb value)"},
	{ATTR_SHOW_CHART_DIVISION_LABELS, "show_chart_division_labels", "int (boolean)"},
	{ATTR_TRACE_BGCOLOR, "trace_bgcolor", "int (rgb value)"},
	{ATTR_PLOT_FONT, "plot_font", "(char *)"},
	{ATTR_PLOT_FONT_NAME_LENGTH, "plot_font_name_length", "int (not settable)"},
	{ATTR_INTERPOLATE_PIXELS, "interpolate_pixels", "int (boolean)"},
	{ATTR_PLOT_ZPLANE_POSITION, "plot_zplane_position", "int (0 - numPlots - 1)"},
	{ATTR_NUM_POINTS, "num_points", "size_t (not settable)"},
	{ATTR_PLOT_XDATA, "plot_xdata", "(void *)"},
	{ATTR_PLOT_YDATA, "plot_ydata", "(void *)"},
	{ATTR_PLOT_ZDATA, "plot_zdata", "(void *)"},
	{ATTR_PLOT_XDATA_TYPE, "plot_xdata_type", "int (not settable)"},
	{ATTR_PLOT_YDATA_TYPE, "plot_ydata_type", "int (not settable)"},
	{ATTR_PLOT_ZDATA_TYPE, "plot_zdata_type", "int (not settable)"},
	{ATTR_PLOT_XDATA_SIZE, "plot_xdata_size", "size_t (not settable)"},
	{ATTR_PLOT_YDATA_SIZE, "plot_ydata_size", "size_t (not settable)"},
	{ATTR_PLOT_ZDATA_SIZE, "plot_zdata_size", "size_t (not settable)"},
	{ATTR_PLOT_YAXIS, "plot_yaxis", "int: VAL_LEFT_YAXIS or VAL_RIGHT_YAXIS"},
	{ATTR_PLOT_SNAPPABLE, "plot_snappable", "int (boolean)"},
	{ATTR_PLOT_ORIGIN, "plot_origin", "int: VAL_LOWER_LEFT,..."},
	{ATTR_PLOT_THICKNESS, "plot_thickness", "int: 1-32 (only for solid lines)"},
	{ATTR_PLOT_XAXIS, "plot_xaxis", "int: VAL_BOTTOM_XAXIS or VAL_TOP_XAXIS"},
	{ATTR_CHECK_MODE, "check_mode", "int (boolean)"},
	{ATTR_CHECK_STYLE, "check_style", "int: VAL_CHECK_MARK, ..."},
	{ATTR_TEXT_CLICK_TOGGLES_CHECK, "text_click_toggles_check", "int: (boolean)"},
	{ATTR_HILITE_CURRENT_ITEM, "hilite_current_item", "int (boolean)"},
	{ATTR_ALLOW_ROOM_FOR_IMAGES, "allow_room_for_images", "int (boolean)"},
	{ATTR_DRAGGABLE_MARKS, "draggable_marks", "int (boolean)"},
	{ATTR_INTERVAL, "interval", "double (seconds)"},
	{ATTR_ENABLED, "enabled", "int (boolean)"},
	{ATTR_PLOT_LG_VISIBLE, "plot_lg_visible", "int (boolean)"},
	{ATTR_PLOT_LG_TEXT, "plot_lg_text", "(char *)"},
	{ATTR_PLOT_LG_TEXT_LENGTH, "plot_lg_text_length", "int (not settable)"},
	{ATTR_PLOT_LG_TEXT_COLOR, "plot_lg_text_color", "int (rgb value)"},
	{ATTR_PLOT_LG_FONT, "plot_lg_font", "(char *)"},
	{ATTR_PLOT_LG_FONT_NAME_LENGTH, "plot_lg_font_name_length", "int (not settable)"},
	{ATTR_FRAME_VISIBLE, "frame_visible", "int (boolean)"},
	{ATTR_PICT_BGCOLOR, "pict_bgcolor", "int (rgb value)"},
	{ATTR_FIT_MODE, "fit_mode", "int: VAL_SIZE_TO_IMAGE, ..."},
	{ATTR_ORIENTATION, "orientation", "int: VAL_PORTRAIT, ..."},
	{ATTR_PRINT_AREA_HEIGHT, "print_area_height", "int (millimeter/10), VAL_USE..."},
	{ATTR_PRINT_AREA_WIDTH, "print_area_width", "int (millimeter/10), VAL_USE..."},
	{ATTR_NUMCOPIES, "numcopies", "int (1-5000)"},
	{ATTR_XRESOLUTION, "xresolution", "int (dpi), ..."},
	{ATTR_YRESOLUTION, "yresolution", "int (dpi), ..."},
	{ATTR_XOFFSET, "xoffset", "int (millimeter/10), VAL_CENTER..."},
	{ATTR_YOFFSET, "yoffset", "int (millimeter/10), VAL_CENTER..."},
	{ATTR_COLOR_MODE, "color_mode", "int: VAL_BW, ..."},
	{ATTR_DUPLEX, "duplex", "int: VAL_SIMPLEX, ..."},
	{ATTR_EJECT_AFTER, "eject_after", "int (boolean)"},
	{ATTR_TEXT_WRAP, "text_wrap", "int (boolean)"},
	{ATTR_TAB_INTERVAL, "tab_interval", "int (1-80)"},
	{ATTR_SHOW_PAGE_NUMBERS, "show_page_numbers", "int (boolean)"},
	{ATTR_SHOW_LINE_NUMBERS, "show_line_numbers", "int (boolean)"},
	{ATTR_SHOW_FILE_NAME, "show_file_name", "int (boolean)"},
	{ATTR_SHOW_DATE, "show_date", "int (boolean)"},
	{ATTR_SHOW_TIME, "show_time", "int (boolean)"},
	{ATTR_PRINT_FONT_NAME, "print_font_name", "(char *)"},
	{ATTR_PRINT_ITALIC, "print_italic", "int (boolean)"},
	{ATTR_PRINT_UNDERLINE, "print_underline", "int (boolean)"},
	{ATTR_PRINT_STRIKEOUT, "print_strikeout", "int (boolean)"},
	{ATTR_PRINT_POINT_SIZE, "print_point_size", "int (6-48)"},
	{ATTR_PRINT_BOLD, "print_bold", "int (boolean)"},
	{ATTR_PRINT_FONT_NAME_LENGTH, "print_font_name_length", "int (not settable)"},
	{ATTR_PRINTER_NAME, "printer_name", "(char *)"},
	{ATTR_PRINTER_NAME_LENGTH, "printer_name_length", "int (not settable)"},
	{ATTR_BITMAP_PRINTING, "bitmap_printing", "int (boolean)"},
	{ATTR_SYSTEM_PRINT_DIALOG_ONLY, "system_print_dialog_only", "int (boolean)"},
	{ATTR_CHARS_PER_LINE, "chars_per_line", "int"},
	{ATTR_LINES_PER_PAGE, "lines_per_page", "int"},
	{ATTR_PEN_COLOR, "pen_color", "int (rgb value)"},
	{ATTR_PEN_FILL_COLOR, "pen_fill_color", "int (rgb value)"},
	{ATTR_PEN_MODE, "pen_mode", "int: VAL_COPY_MODE, ..."},
	{ATTR_PEN_WIDTH, "pen_width", "int: (1-255)"},
	{ATTR_PEN_PATTERN, "pen_pattern", "unsigned char[8]"},
	{ATTR_PEN_STYLE, "pen_style", "int: VAL_SOLID, ..."},
	{ATTR_DRAW_POLICY, "draw_policy", "int: VAL_UPDATE_IMMEDIATELY, ..."},
	{ATTR_OVERLAPPED, "overlapped", "int (not settable)"},
	{ATTR_OVERLAPPED_POLICY, "overlapped_policy", "int: VAL_DRAW_ON_TOP or VAL_DEFER_DRAWING"},
	{ATTR_XCOORD_AT_ORIGIN, "xcoord_at_origin", "double"},
	{ATTR_YCOORD_AT_ORIGIN, "ycoord_at_origin", "double"},
	{ATTR_XSCALING, "xscaling", "double"},
	{ATTR_YSCALING, "yscaling", "double"},
	{ATTR_ALLOW_UNSAFE_TIMER_EVENTS, "allow_unsafe_timer_events", "int (boolean)"},
	{ATTR_REPORT_LOAD_FAILURE, "report_load_failure", "int (boolean)"},
	{ATTR_ALLOW_MISSING_CALLBACKS, "allow_missing_callbacks", "int (boolean)"},
	{ATTR_SUPPRESS_EVENT_PROCESSING, "suppress_event_processing", "int (boolean)"},
	{ATTR_TASKBAR_BUTTON_VISIBLE, "taskbar_button_visible", "int (boolean)"},
	{ATTR_TASKBAR_BUTTON_TEXT, "taskbar_button_text", "(char *)     "},
	{ATTR_DEFAULT_MONITOR, "default_monitor", "int"},
	{ATTR_PRIMARY_MONITOR, "primary_monitor", "int"},
	{ATTR_NUM_MONITORS, "num_monitors", "int"},
	{ATTR_FIRST_MONITOR, "first_monitor", "int"},
	{ATTR_DISABLE_PROG_PANEL_SIZE_EVENTS, "disable_prog_panel_size_events", "int (boolean)"},
	{ATTR_USE_LOCALIZED_DECIMAL_SYMBOL, "use_localized_decimal_symbol", "int: VAL_USE_PERIOD, VAL_USE_SYSTEM_SETTING"},
	{ATTR_LOCALIZED_DECIMAL_SYMBOL, "localized_decimal_symbol", "char (not settable)"},
	{ATTR_MINIMIZE_PANELS_TO_DESKTOP, "minimize_panels_to_desktop", "int (boolean)"},
	{ATTR_USE_CHECKERBOARD_DIMMING, "use_checkerboard_dimming", "int (boolean)"},
	{ATTR_RESOLUTION_ADJUSTMENT, "resolution_adjustment", "int: VAL_USE_PANEL_SETTING, 0-100"},
	{ATTR_UPPER_LEFT_CORNER_COLOR, "upper_left_corner_color", "int (rgb value)"},
	{ATTR_ROW_LABELS_COLOR, "row_labels_color", "int (rgb value)"},
	{ATTR_COLUMN_LABELS_COLOR, "column_labels_color", "int (rgb value)"},
	{ATTR_TABLE_BGCOLOR, "table_bgcolor", "int (rgb value)"},
	{ATTR_TABLE_MODE, "table_mode", "int: VAL_COLUMN, ..."},
	{ATTR_ROW_LABELS_VISIBLE, "row_labels_visible", "int (boolean)"},
	{ATTR_COLUMN_LABELS_VISIBLE, "column_labels_visible", "int (boolean)"},
	{ATTR_ROW_LABELS_WIDTH, "row_labels_width", "int (0-32767)"},
	{ATTR_COLUMN_LABELS_HEIGHT, "column_labels_height", "int (0-32767)"},
	{ATTR_FIRST_VISIBLE_ROW, "first_visible_row", "int"},
	{ATTR_FIRST_VISIBLE_COLUMN, "first_visible_column", "int"},
	{ATTR_NUM_VISIBLE_ROWS, "num_visible_rows", "int"},
	{ATTR_NUM_VISIBLE_COLUMNS, "num_visible_columns", "int"},
	{ATTR_ENABLE_ROW_SIZING, "enable_row_sizing", "int (boolean)"},
	{ATTR_ENABLE_COLUMN_SIZING, "enable_column_sizing", "int (boolean)"},
	{ATTR_ENABLE_POPUP_MENU, "enable_popup_menu", "int (boolean)"},
	{ATTR_GRID_AREA_TOP, "grid_area_top", "int (not settable)"},
	{ATTR_GRID_AREA_LEFT, "grid_area_left", "int (not settable)"},
	{ATTR_GRID_AREA_WIDTH, "grid_area_width", "int (not settable)"},
	{ATTR_GRID_AREA_HEIGHT, "grid_area_height", "int (not settable)"},
	{ATTR_TABLE_RUN_STATE, "table_run_state", "int: VAL_SELECT_STATE, ..."},
	{ATTR_AUTO_EDIT, "auto_edit", "int (boolean)"},
	{ATTR_SNAP_COLUMN_WIDTHS_TO_CTRL_WIDTH, "snap_column_widths_to_ctrl_width", "int (boolean)"},
	{ATTR_CELL_TYPE, "cell_type", "int: VAL_CELL_NUMERIC, ..."},
	{ATTR_CELL_DIMMED, "cell_dimmed", "int (boolean)"},
	{ATTR_CELL_MODE, "cell_mode", "int: VAL_HOT, ..."},
	{ATTR_HORIZONTAL_GRID_COLOR, "horizontal_grid_color", "int (rgb value)"},
	{ATTR_VERTICAL_GRID_COLOR, "vertical_grid_color", "int (rgb value)"},
	{ATTR_HORIZONTAL_GRID_VISIBLE, "horizontal_grid_visible", "int (boolean)"},
	{ATTR_VERTICAL_GRID_VISIBLE, "vertical_grid_visible", "int (boolean)"},
	{ATTR_MIN_NUM_LINES_VISIBLE, "min_num_lines_visible", "int"},
	{ATTR_INCDEC_ARROW_COLOR, "incdec_arrow_color", "int (rgb value)"},
	{ATTR_NUM_CELL_DFLT_VALUE, "num_cell_dflt_value", "same datatype as cell"},
	{ATTR_STR_CELL_DFLT_VALUE, "str_cell_dflt_value", "(char *)"},
	{ATTR_STR_CELL_DFLT_VALUE_LENGTH, "str_cell_dflt_value_length", "int (not settable)"},
	{ATTR_CELL_JUSTIFY, "cell_justify", "int: VAL_TOP_LEFT_JUSTIFIED, ..."},
	{ATTR_STR_CELL_NUM_LINES, "str_cell_num_lines", "int (not settable)"},
	{ATTR_CELL_FRAME_COLOR, "cell_frame_color", "int (rgb value)"},
	{ATTR_SHOW_RING_ARROW, "show_ring_arrow", "int (boolean)"},
	{ATTR_RING_ARROW_LOCATION, "ring_arrow_location", "int: VAL_LEFT_ANCHOR, VAL_RIGHT_ANCHOR"},
	{ATTR_RING_ITEMS_UNIQUE, "ring_items_unique", "int (boolean)"},
	{ATTR_CASE_SENSITIVE_COMPARE, "case_sensitive_compare", "int (boolean)"},
	{ATTR_CELL_SHORTCUT_KEY, "cell_shortcut_key", "int"},
	{ATTR_USE_LABEL_TEXT, "use_label_text", "int (boolean)"},
	{ATTR_SIZE_MODE, "size_mode", "int: VAL_USE_EXPLICIT_SIZE, ..."},
	{ATTR_LABEL_WRAP_MODE, "label_wrap_mode", "int: VAL_CHAR_WRAP, ..."},
	{ATTR_ROW_HEIGHT, "row_height", "int (1-32767)"},
	{ATTR_ROW_ACTUAL_HEIGHT, "row_actual_height", "int (not settable)"},
	{ATTR_COLUMN_WIDTH, "column_width", "int (1-32767)"},
	{ATTR_COLUMN_ACTUAL_WIDTH, "column_actual_width", "int (not settable)"},
	{ATTR_COLUMN_VISIBLE, "column_visible", "int (boolean)"},
	{ATTR_DATASOCKET_ENABLED, "datasocket_enabled", "int (boolean)"},
	{ATTR_DS_BIND_PLOT_STYLE, "ds_bind_plot_style", "int: VAL_THIN_LINE, ..."},
	{ATTR_DS_BIND_POINT_STYLE, "ds_bind_point_style", "int: VAL_EMPTY_SQUARE, ..."},
	{ATTR_DS_BIND_LINE_STYLE, "ds_bind_line_style", "int: VAL_SOLID, ..."},
	{ATTR_DS_BIND_PLOT_COLOR, "ds_bind_plot_color", "int (rgb value)"},
	{ATTR_DATASOCKET_SOURCE, "datasocket_source", "(char *) (not settable)"},
	{ATTR_DATASOCKET_SOURCE_LENGTH, "datasocket_source_length", "int (not settable)"},
	{ATTR_DATASOCKET_MODE, "datasocket_mode", "int (not settable)"},
	{ATTR_COLOR_DEPTH, "color_depth", "int"},
	{ATTR_SYSTEM_MONITOR_HANDLE, "system_monitor_handle", "intptr_t (HMONITOR) (not settable)"},
	{ATTR_NEXT_MONITOR, "next_monitor", "int"},
	{ATTR_POPUP_STYLE, "popup_style", "int: VAL_CLASSIC, ..."},
	{ATTR_TREE_BGCOLOR, "tree_bgcolor", "int (rgb value)"},
	{ATTR_SHOW_CONNECTION_LINES, "show_connection_lines", "int (boolean)"},
	{ATTR_SHOW_PLUS_MINUS, "show_plus_minus", "int (boolean)"},
	{ATTR_SHOW_MARKS, "show_marks", "int (boolean)"},
	{ATTR_SHOW_IMAGES, "show_images", "int (boolean)"},
	{ATTR_MARK_REFLECT, "mark_reflect", "int (boolean)"},
	{ATTR_AUTO_EXPAND, "auto_expand", "int (boolean)"},
	{ATTR_AUTO_HSCROLL, "auto_hscroll", "int (boolean)"},
	{ATTR_FULL_ROW_SELECT, "full_row_select", "int (boolean)"},
	{ATTR_INDENT_OFFSET, "indent_offset", "int (0-32767 in pixels)"},
	{ATTR_RADIO_SIBLING_ALWAYS_MARKED, "radio_sibling_always_marked", "int (boolean)"},
	{ATTR_TEXT_CLICK_TOGGLES_MARK, "text_click_toggles_mark", "int (boolean)"},
	{ATTR_HIDE_ACTIVE_ITEM, "hide_active_item", "int (boolean)"},
	{ATTR_SELECTION_MODE, "selection_mode", "int: VAL_SELECTION_MULTIPLE, ..."},
	{ATTR_TREE_RUN_STATE, "tree_run_state", "int: VAL_SELECT_STATE, ..."},
	{ATTR_ENABLE_DRAG_DROP, "enable_drag_drop", "int (boolean)"},
	{ATTR_EXPANDED_IMAGE_INDEX, "expanded_image_index", "int"},
	{ATTR_DISABLE_TOOLTIPS, "disable_tooltips", "int (boolean)"},
	{ATTR_HIDE_ACTIVE_ITEM_ALWAYS, "hide_active_item_always", "int (boolean)"},
	{ATTR_HIDE_HILITE, "hide_hilite", "int (boolean)"},
	{ATTR_TREE_EDITABLE_CELLS, "tree_editable_cells", "int (boolean)"},
	{ATTR_COLOR_PICKER_VALUE, "color_picker_value", "int (rgb value)"},
//	{ATTR_HORIZONTAL_BAR_VALUE, "horizontal_bar_value", "double (0.0-100.0)"},
	{ATTR_HORIZONTAL_BAR_COLOR, "horizontal_bar_color", "int (rgb value)"},
	{ATTR_COLLAPSED_IMAGE_INDEX, "collapsed_image_index", "int"},
	{ATTR_MARK_TYPE, "mark_type", "int: VAL_MARK_NONE, ..."},
	{ATTR_MARK_STATE, "mark_state", "int: VAL_MARK_OFF, ..."},
	{ATTR_SELECTED, "selected", "int (boolean)"},
	{ATTR_COLLAPSED, "collapsed", "int (boolean)"},
	{ATTR_NO_EDIT_LABEL, "no_edit_label", "int (boolean)"},
	{ATTR_ITEM_HEIGHT, "item_height", "int"},
	{ATTR_ITEM_ACTUAL_HEIGHT, "item_actual_height", "int"},
	{ATTR_ENABLE_DRAG, "enable_drag", "int (boolean)"},
	{ATTR_ENABLE_DROP, "enable_drop", "int (boolean)"},
	{ATTR_IMAGE_INDEX, "image_index", "int"},
	{ATTR_HILITE_ONLY_WHEN_PANEL_ACTIVE, "hilite_only_when_panel_active", "int (boolean)"},
	{ATTR_TITLEBAR_STYLE, "titlebar_style", "int: VAL_CLASSIC, ..."},
	{ATTR_ITEM_BITMAP, "item_bitmap", "int (bitmap id)"},
	#define ATTR_DOUBLE_CLICK_TOGGLES_COLLAPSE          1993
	{ATTR_TOP_RANGE, "top_range", "int (-32768 - 32767, VAL_AUTO_RANGE, VAL_NO_RANGE_LIMIT)"},
	{ATTR_BOTTOM_RANGE, "bottom_range", "int (-32768 - 32767, VAL_AUTO_RANGE, VAL_NO_RANGE_LIMIT)"},
	{ATTR_LEFT_RANGE, "left_range", "int (-32768 - 32767, VAL_AUTO_RANGE, VAL_NO_RANGE_LIMIT)"},
	{ATTR_RIGHT_RANGE, "right_range", "int (-32768 - 32767, VAL_AUTO_RANGE, VAL_NO_RANGE_LIMIT)"},
	{ATTR_SHOW_CONTENTS_WHILE_DRAGGING, "show_contents_while_dragging", "int (boolean)"},
	{ATTR_SPAN_PANEL, "span_panel", "int (boolean)"},
	{ATTR_OPERABLE_AS_INDICATOR, "operable_as_indicator", "int (boolean)"},
	{ATTR_TOP_ACTUAL_RANGE, "top_actual_range", "int (not settable)"},
	{ATTR_BOTTOM_ACTUAL_RANGE, "bottom_actual_range", "int (not settable)"},
	{ATTR_LEFT_ACTUAL_RANGE, "left_actual_range", "int (not settable)"},
	{ATTR_RIGHT_ACTUAL_RANGE, "right_actual_range", "int (not settable)"},
	{ATTR_DIGWAVEFORM_AUTOSIZE, "digwaveform_autosize", "int (boolean)"},
	{ATTR_DIGWAVEFORM_LINE_LABEL, "digwaveform_line_label", "(char *)"},
	{ATTR_DIGWAVEFORM_LINE_LABEL_LENGTH, "digwaveform_line_label_length", "int (not settable)"},
	{ATTR_DIGWAVEFORM_BUS_LABEL, "digwaveform_bus_label", "(char *)"},
	{ATTR_DIGWAVEFORM_BUS_LABEL_LENGTH, "digwaveform_bus_label_length", "int (not settable)"},
	{ATTR_DIGWAVEFORM_FONT, "digwaveform_font", "(char *)"},
	{ATTR_DIGWAVEFORM_FONT_NAME_LENGTH, "digwaveform_font_name_length", "int (not settable)"},
	{ATTR_DIGWAVEFORM_SHOW_STATE_LABEL, "digwaveform_show_state_label", "int (boolean)"},
	{ATTR_DIGWAVEFORM_EXPAND_BUSES, "digwaveform_expand_buses", "int (boolean)"},
	{ATTR_DIGWAVEFORM_PLOT_COLOR, "digwaveform_plot_color", "int (rgb value)"},
	{ATTR_TABS_FIT_MODE, "tabs_fit_mode", "int: VAL_SINGLE_ROW, ..."},
	{ATTR_TABS_LOCATION, "tabs_location", "int: VAL_LEFT_TOP, ..."},
	{ATTR_TABS_VISIBLE, "tabs_visible", "int (boolean)"},
	{ATTR_TABS_START_OFFSET, "tabs_start_offset", "int (-32768 - 32767)"},
	{ATTR_TABS_END_OFFSET, "tabs_end_offset", "int (-32768 - 32767)"},
	{ATTR_TABS_MAX_TAB_WIDTH, "tabs_max_tab_width", "int (-32768 - 32767)"},
	{ATTR_NUM_ANNOTATIONS, "num_annotations", "int (0-10)"},
	{ATTR_ANNOTATION_CAPTION, "annotation_caption", "(char *)"},
	{ATTR_ANNOTATION_CAPTION_LENGTH, "annotation_caption_length", "int (not settable)"},
	{ATTR_ANNOTATION_CAPTION_FONT, "annotation_caption_font", "(char *)"},
	{ATTR_ANNOTATION_CAPTION_FONT_NAME_LENGTH, "annotation_caption_font_name_length", "int (not settable)"},
	{ATTR_ANNOTATION_CAPTION_COLOR, "annotation_caption_color", "int (rgb value)"},
	{ATTR_ANNOTATION_CAPTION_BGCOLOR, "annotation_caption_bgcolor", "int (rgb value)"},
	{ATTR_ANNOTATION_CAPTION_BOLD, "annotation_caption_bold", "int (boolean)"},
	{ATTR_ANNOTATION_CAPTION_ITALIC, "annotation_caption_italic", "int (boolean)"},
	{ATTR_ANNOTATION_CAPTION_UNDERLINE, "annotation_caption_underline", "int (boolean)"},
	{ATTR_ANNOTATION_CAPTION_STRIKEOUT, "annotation_caption_strikeout", "int (boolean)"},
	{ATTR_ANNOTATION_CAPTION_POINT_SIZE, "annotation_caption_point_size", "int (1-255)"},
	{ATTR_ANNOTATION_CAPTION_ANGLE, "annotation_caption_angle", "int (-32768 - 32767)"},
	{ATTR_ANNOTATION_CAPTION_CHARACTER_SET, "annotation_caption_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_ANNOTATION_GLYPH_STYLE, "annotation_glyph_style", "int: VAL_EMPTY_SQUARE, ..."},
	{ATTR_ANNOTATION_GLYPH_COLOR, "annotation_glyph_color", "int (rgb value)"},
	{ATTR_ANNOTATION_LINE_STYLE, "annotation_line_style", "int: VAL_THIN_LINE, ..."},
	{ATTR_ANNOTATION_LINE_COLOR, "annotation_line_color", "int (rgb value)"},
	{ATTR_ANNOTATION_ARROW_STYLE, "annotation_arrow_style", "int: VAL_ARROW, ..."},
	{ATTR_ANNOTATION_XVALUE, "annotation_xvalue", "double"},
	{ATTR_ANNOTATION_YVALUE, "annotation_yvalue", "double"},
	{ATTR_ANNOTATION_XOFFSET, "annotation_xoffset", "int"},
	{ATTR_ANNOTATION_YOFFSET, "annotation_yoffset", "int"},
	{ATTR_ANNOTATION_LOCKED, "annotation_locked", "int (boolean)"},
	{ATTR_ANNOTATION_XAXIS, "annotation_xaxis", "int: VAL_BOTTOM_XAXIS or VAL_TOP_XAXIS"},
	{ATTR_ANNOTATION_YAXIS, "annotation_yaxis", "int: VAL_LEFT_YAXIS or VAL_RIGHT_YAXIS"},
	{ATTR_ANNOTATION_VISIBLE, "annotation_visible", "int (boolean)"},
	{ATTR_ANNOTATION_HIDE_CAPTION, "annotation_hide_caption", "int (boolean)"},
	{ATTR_ANNOTATION_CAPTION_ALWAYS_IN_VIEW, "annotation_caption_always_in_view", "int (boolean)"},
	{ATTR_ANNOTATION_OFFSET_MODE, "annotation_offset_mode", "int: VAL_GLYPH_OFFSET or VAL_TOP_LEFT_OFFSET"},
	{ATTR_COLOR_RAMP_WIDTH, "color_ramp_width", "int (1-255)"},
	{ATTR_COLOR_RAMP_INTERPOLATE, "color_ramp_interpolate", "int (boolean)"},
	{ATTR_NUM_COLOR_RAMP_VALUES, "num_color_ramp_values", "int (not settable)"},
	{ATTR_XLABEL_FONT, "xlabel_font", "(char *)"},
	{ATTR_XLABEL_COLOR, "xlabel_color", "int (rgb value)"},
	{ATTR_XLABEL_BOLD, "xlabel_bold", "int (boolean)"},
	{ATTR_XLABEL_ANGLE, "xlabel_angle", "int (-32768 - 32767)"},
	{ATTR_XLABEL_ITALIC, "xlabel_italic", "int (boolean)"},
	{ATTR_XLABEL_UNDERLINE, "xlabel_underline", "int (boolean)"},
	{ATTR_XLABEL_STRIKEOUT, "xlabel_strikeout", "int (boolean)"},
	{ATTR_XLABEL_POINT_SIZE, "xlabel_point_size", "int (1-255)"},
	{ATTR_XLABEL_FONT_NAME_LENGTH, "xlabel_font_name_length", "int (not settable)"},
	{ATTR_XLABEL_CHARACTER_SET, "xlabel_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_YLABEL_FONT, "ylabel_font", "(char *)"},
	{ATTR_YLABEL_COLOR, "ylabel_color", "int (rgb value)"},
	{ATTR_YLABEL_BOLD, "ylabel_bold", "int (boolean)"},
	{ATTR_YLABEL_ANGLE, "ylabel_angle", "int (-32768 - 32767)"},
	{ATTR_YLABEL_ITALIC, "ylabel_italic", "int (boolean)"},
	{ATTR_YLABEL_UNDERLINE, "ylabel_underline", "int (boolean)"},
	{ATTR_YLABEL_STRIKEOUT, "ylabel_strikeout", "int (boolean)"},
	{ATTR_YLABEL_POINT_SIZE, "ylabel_point_size", "int (1-255)"},
	{ATTR_YLABEL_FONT_NAME_LENGTH, "ylabel_font_name_length", "int (not settable)"},
	{ATTR_YLABEL_CHARACTER_SET, "ylabel_character_set", "int: VAL_ANSI_CHARSET, ..."},
	{ATTR_NUM_PLOTS, "num_plots", "int (not settable)"},
	{ATTR_FIRST_PLOT, "first_plot", "int (plot Id) (not settable)"},
	{ATTR_NEXT_PLOT, "next_plot", "int (plot Id) (not settable)"},
	{ATTR_USE_PROGRESS_BAR_VISUAL_STYLES, "use_progress_bar_visual_styles", "int (boolean)"}
};
#pragma clang diagnostic pop

static int PanelAttributes[]={
		ATTR_BACKCOLOR,
		ATTR_DIMMED,
		ATTR_FRAME_ACTUAL_HEIGHT,
		ATTR_FRAME_ACTUAL_WIDTH,
		ATTR_FRAME_COLOR,
		ATTR_FRAME_STYLE,
		ATTR_FRAME_THICKNESS,
		ATTR_HSCROLL_OFFSET,
		ATTR_HSCROLL_OFFSET_MAX,
		ATTR_MENU_HEIGHT,
		ATTR_MENU_WIDTH,
		ATTR_MENU_BAR_VISIBLE,
		ATTR_SCROLL_BAR_COLOR,
		ATTR_SCROLL_BAR_SIZE,
		ATTR_SCROLL_BAR_STYLE,
		ATTR_SCROLL_BARS,
		ATTR_SYSTEM_MENU_VISIBLE,
		ATTR_VSCROLL_OFFSET,
		ATTR_VSCROLL_OFFSET_MAX,
		ATTR_VISIBLE,
		ATTR_WINDOW_ZOOM,
		ATTR_HEIGHT,
		ATTR_LEFT,
		ATTR_TOP,
		ATTR_WIDTH,
		ATTR_ACTIVATE_WHEN_CLICKED_ON,
		ATTR_CAN_MAXIMIZE,
		ATTR_CAN_MINIMIZE,
		ATTR_CLOSE_CTRL,
		ATTR_CLOSE_ITEM_VISIBLE,
		ATTR_CONFORM_TO_SYSTEM,
		ATTR_CONFORM_TO_SYSTEM_THEME,
		ATTR_FIRST_CHILD,
		ATTR_PANEL_FIRST_CTRL,
		ATTR_FLOATING,
		ATTR_HAS_TASKBAR_BUTTON,
		ATTR_PANEL_MENU_BAR_CONSTANT,
		ATTR_PANEL_MENU_BAR_CONSTANT_LENGTH,
		ATTR_MINIMIZE_OTHER_PANELS,
		ATTR_MIN_HEIGHT_FOR_SCALING,
		ATTR_MIN_WIDTH_FOR_SCALING,
		ATTR_MOUSE_CURSOR,
		ATTR_MOVABLE,
		ATTR_NEXT_PANEL,
		ATTR_NUM_CHILDREN,
		ATTR_NUM_CTRLS,
		ATTR_OWNER_THREAD_ID,
		ATTR_ACTIVE,
		ATTR_PANEL_PARENT,
		ATTR_PARENT_SHARES_SHORTCUT_KEYS,
		ATTR_RESOLUTION_ADJUSTMENT,
		ATTR_SCALE_CONTENTS_ON_RESIZE,
		ATTR_SIZABLE,
		ATTR_SYSTEM_WINDOW_HANDLE,
		ATTR_ZPLANE_POSITION,
		ATTR_CALLBACK_DATA,
		ATTR_CALLBACK_NAME,
		ATTR_CALLBACK_NAME_LENGTH,
		ATTR_CALLBACK_FUNCTION_POINTER,
		ATTR_CONSTANT_NAME,
		ATTR_CONSTANT_NAME_LENGTH,
		ATTR_TITLE,
		ATTR_TITLE_BACKCOLOR,
		ATTR_TITLE_BOLD,
		ATTR_TITLE_CHARACTER_SET,
		ATTR_TITLE_COLOR,
		ATTR_TITLE_FONT,
		ATTR_TITLE_FONT_NAME_LENGTH,
		ATTR_TITLE_ITALIC,
		ATTR_TITLE_LENGTH,
		ATTR_TITLE_POINT_SIZE,
		ATTR_TITLE_SIZE_TO_FONT,
		ATTR_TITLE_STRIKEOUT,
		ATTR_TITLE_UNDERLINE,
		ATTR_TITLEBAR_ACTUAL_THICKNESS,
		ATTR_TITLEBAR_STYLE,
		ATTR_TITLEBAR_THICKNESS,
		ATTR_TITLEBAR_VISIBLE
	};

static int IdxOfAttr[3000]={0};

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return TRUE if an attribute is a valid panel attribute
///////////////////////////////////////////////////////////////////////////////
static int IsPanelAttribute(int Attr) {
	unsigned int i;
	for (i=0; i<sizeof(PanelAttributes)/sizeof(int); i++) 
		if (Attr==PanelAttributes[i]) 
			return TRUE;
	return FALSE;
}

// Call this once, before doing anything else
static void DefinePanelAttributes(void) {
	unsigned int i;
	for (i=0; i<sizeof(AttrList)/sizeof(tAttrList); i++) {
		AttrList[i].IsPanelAttr=IsPanelAttribute(AttrList[i].Attribute);
		IdxOfAttr[AttrList[i].Attribute]=i;
	}
}


///////////////////////////////////////////////////////////////////////////////
// Compare first characters
#define SAME(S) (strncmp(AttrItem->TypeStr, S, strlen(S))==0)

#define GETS { /* Get a string attribute */ \
		if (Control==0) r=GetPanelAttribute(Panel, AttrItem->Attribute, Str);\
		else            r=GetCtrlAttribute(Panel, Control, AttrItem->Attribute, Str);\
		if (r==0) snprintf(Dest, Size, "\n%s\t%s", AttrItem->NameStr, Str);\
		return r==0;\
	}

#define GETPtr { /* Get a pointer attribute and show its address */ \
		if (Control==0) r=GetPanelAttribute(Panel, AttrItem->Attribute, (void*)&I);\
		else            r=GetCtrlAttribute(Panel, Control, AttrItem->Attribute, (void*)&I);\
		if (r==0) snprintf(Dest, Size, "\n%s\t%#x", AttrItem->NameStr, I);\
		return r==0;\
	}

#define GETB { /* Get an int and show as a boolean */\
		if (Control==0) r=GetPanelAttribute(Panel, AttrItem->Attribute, &I);\
		else            r=GetCtrlAttribute(Panel, Control, AttrItem->Attribute, &I);\
		if (r==0) snprintf(Dest, Size, "\n%s\t%s", AttrItem->NameStr, I?"TRUE":"FALSE");\
		return r==0;\
	}

#define GET(What, Frmt) { /* Get anything */\
		if (Control==0) r=GetPanelAttribute(Panel, AttrItem->Attribute, &What);\
		else            r=GetCtrlAttribute(Panel, Control, AttrItem->Attribute, &What);\
		if (r==0) snprintf(Dest, Size, "\n%s\t" Frmt, AttrItem->NameStr, What);\
		return r==0;\
	}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Show an attribute, if available
/// HIRET	TRUE is the argument is being shown
// Get the different types with the following command:
// grep "#define ATTR_" userint.h | tr "\t" " " | grep "/\*" | sed -e "s%.*/\*%%" -e "s/: VAL_.*//" -e "s% \*/%%" | sort | uniq -c | sort -n
///////////////////////////////////////////////////////////////////////////////
static BOOL GetAttribute(char* Dest, int Size, int Panel, int Control, tAttrList *AttrItem) {
	char Str[1024], C;	// Buffer overflow likely
	unsigned char UC;
	double D;
	float F;
	int r, I, CtrlType=VAL_NO_TYPE;
	unsigned int U;
	long long L;
	unsigned long long UL;
	short S;
	unsigned short US;
	
	
	Dest[0]='\0';
	
	if (AttrItem->Attribute<FirstAttr or LastAttr<AttrItem->Attribute or 
		AttrItem->NameStr==NULL or AttrItem->TypeStr==NULL) 
		return FALSE;	// wrong attribute
	
	if (Control==0 and !AttrItem->IsPanelAttr) return FALSE;
	
	// Compare the begining of the string, in order
	if  SAME("(char *)")		GETS;
	if  SAME("char")			GET(C, "%c");
	if  SAME("double")			GET(D, "%g");
	if (SAME("int: (boolean)") or
		SAME("int (boolean)"))	GETB;
	if (SAME("intptr_t") or
		SAME("(void *)"))		GETPtr;
	if  SAME("int (rgb value)") GET(I, "%#.8x");
	if (SAME("size_t") or
		SAME("int"))			GET(I, "%d");
	if  SAME("same datatype as ctrl") {	// Does not happen for panels
		GetCtrlAttribute (Panel, Control, ATTR_DATA_TYPE, &CtrlType);
		switch (CtrlType) {	// breaks are not necessary
			case VAL_STRING:			GETS;
			case VAL_CHAR:				GET(C, "%hhd");
			case VAL_UNSIGNED_CHAR:		GET(UC, "%hhu");

			case VAL_SHORT_INTEGER:		GET(S, "%hd");
			case VAL_UNSIGNED_SHORT_INTEGER:
										GET(US, "%hu");
			
			case VAL_INTEGER:			GET(I, "%d");
			case VAL_UNSIGNED_INTEGER:	GET(U, "%u");
			
			case VAL_64BIT_INTEGER:		GET(L, "%lld");
			case VAL_UNSIGNED_64BIT_INTEGER:
										GET(UL, "%llu");

			case VAL_FLOAT:				GET(F, "%g");
			case VAL_DOUBLE:			GET(D, "%g");
			
			case VAL_NO_TYPE:			/*fprintf(File, "\n%s\t%s\t-No type-", AttrName, Type);*/ 
										return FALSE;

//			case VAL_SSIZE_T:
//			case VAL_INTPTR_T:
//			case VAL_UINTPTR_T:
//			case VAL_SIZE_T:
//			case VAL_PTRDIFF_T:
//			case VAL_STRING:	
//				break;	// Not handled here. Impossible (?). Only numerics have variable type (?)
		}
	}
	
	
	snprintf(Dest, Size, "\n%s\t%s\t- type %d not handled -", 
		AttrItem->NameStr, AttrItem->TypeStr, CtrlType);
	return TRUE;
}

// Wrapper macro
#define SHOW_ATTR(Attr, Type) ShowAttribute(File, Panel, Control, ATTR_##Attr, #Attr, Type)

static BOOL FirstTime=TRUE;

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Show all available attributes for a given control or panel
/// HIPAR	File/Use stderr/stdout for standard output
/// HIPAR	Control/If 0, show attributes for the panel
///////////////////////////////////////////////////////////////////////////////
void ShowAttributes(FILE* File, int Panel, int Control) {
	char Str[80];
	unsigned int i;
	int B=SetBreakOnLibraryErrors(0);
	
	if (FirstTime) { FirstTime=FALSE; DefinePanelAttributes(); }

	fprintf(File, "\n\nAttribute dump for panel:%d, control:%d", Panel, Control);
	
	for (i=0; i<sizeof(AttrList)/sizeof(tAttrList); i++)
		if (GetAttribute(Str, 80, Panel, Control, &AttrList[i]))
			fputs(Str, File);
	SetBreakOnLibraryErrors(B);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Same as above but in a string
/// HIPAR	Control/If 0, show attributes for the panel
///////////////////////////////////////////////////////////////////////////////
void GetAttributes(char* Dest, unsigned int Size, int Panel, int Control) {
	char Str[80];
	unsigned int i;
	int B=SetBreakOnLibraryErrors(0);
	
	if (FirstTime) { FirstTime=FALSE; DefinePanelAttributes(); }

	snprintf(Dest, Size, "Attribute dump for panel:%d, control:%d", Panel, Control);
	
	for (i=0; i<sizeof(AttrList)/sizeof(tAttrList); i++)
		if (GetAttribute(Str, 80, Panel, Control, &AttrList[i])) {
			if (strlen(Dest)+1+strlen(Str)<Size) 
				strcat(Dest, Str);
			else break;	// String overflow
		}
	SetBreakOnLibraryErrors(B);
}

